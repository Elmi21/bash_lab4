#!/bin/bash
file_out="/home/elmira/file_lab4.txt" #файл для вывода из крона
DISPLAY=:0
export DISPLAY
show_all_tasks(){
	echo "Все задания на данный момент:";
	num_lines=0;
	crontab -l > $file_out
	while read line
	do
	(echo "$line" | grep "/home/elmira/lab4.sh") > /dev/null;
	result=$?;
	if (($result==1))
	then
		continue;
	fi
	local print_line=$(echo "$line" | sed "s/\(.\{,9\}\).*/\1/");
	case "$print_line" in
	'* * * * *' )
	echo "раз в минуту";
	;;
	'0 * * * *' )
	echo "каждый час";
	;;
	'0 0 * * *' )
	echo "раз в день";
	;;
	'0 0 * * 0' )
 	echo "раз в неделю";
 	;;
 	'0 0 1 * *' )
 	echo "раз в месяц";
 	;;
esac
	line=$(echo "$line" |  sed  "s!\*!\\\*!g; s!\ !\\\ !g; s!\/!\\\/!g");
	local_mas_str[num_lines]=`sed -n "/$line/=" $file_out`;
	num_lines=$(( $num_lines+1 ));
	done < $file_out
	eval "$1=( ${local_mas_str[@]} )";
}
select_period(){
	echo "1 - раз в минуту"
	echo "2 - каждый час"
	echo "3 - раз в день"
	echo "4 - раз в неделю"
	echo "5 - раз в месяц"
	read itemsub
	case $itemsub in
	'1' )
	local_period='* * * * *';
	;;
	'2' )
	local_period='0 * * * *';
	;;
	'3' )
	local_period='0 0 * * *';
	;;
	'4' )
 	local_period='0 0 * * 0';
 	;;
 	'5' )
 	local_period='0 0 1 * *';
 	;;
esac
eval "$1=\"$local_period\"";
}
if [[ $# -eq 1 && "$1" = "user" ]]
	then
echo "Контроль выполнения процесса."
echo "Введите имя процесса в переменную PROCESS: "
sudo subl /etc/environment;
while [ 1 ];
do
echo "Выберите пункт меню:";
echo "1 - Добавить задание контроля.";
echo "2 - Редактировать задание контроля.";
echo "3 - Удалить задание контроля.";
echo "4 - показать все задания контроля";
echo "5 - выход"
read item;
if (($item==1))
then
	echo "Введите периодичность проверки выполнения процесса:"
	select_period period
	cron_str="$period /home/elmira/lab4.sh"
	crontab -l | { cat; echo -e "$cron_str\n"; } | crontab
	echo "Задание добавлено!";
fi
if (($item==4))
then
	show_all_tasks mas_str;
fi
if (($item==2))
then
	show_all_tasks mas_str;
	echo "Выберите задание, которое хотите редактировать:";
	read num_task;
	num_task=$num_task-1;
	echo "Введите новую периодичность проверки процесса";
	select_period period
	cron_str="$period /home/elmira/lab4.sh";
	cron_str=$(echo "$cron_str" |  sed  "s!\*!\\\*!g; s!\ !\\\ !g; s!\/!\\\/!g");
	sed "${mas_str[$num_task]} s/.*/$cron_str/i" $file_out | crontab;
	echo "Задание было изменено.";
fi
if (($item==3))
then
	show_all_tasks mas_str;
	echo "Выберите задание, которое хотите удалить:";
	read num_task;
	num_task=$num_task-1;
	sed "${mas_str[$num_task]}d" $file_out | crontab;
	echo "Задание было удалено.";
fi
if (($item==5))
then
	break;
fi
done
else
(ps -A | grep $PROCESS) > /dev/null;
result=$?;
if [ "$result" -eq 1 ]
	then
	subl
fi
fi


